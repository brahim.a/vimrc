" Vim plug manager auto install
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin('~/.vim/plugged')

Plug 'morhetz/gruvbox'
Plug 'flrnd/candid.vim'
Plug 'mhinz/vim-startify'
Plug 'itchyny/lightline.vim'
"Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-surround'
Plug 'preservim/nerdtree'
Plug 'sudar/vim-arduino-syntax'

call plug#end()

"autocmd StdinReadPre * let s:std_in=1
"autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif

syntax on
colorscheme candid " ## gruvbox | candid
set termguicolors
set t_Co=256
"set mouse=a
set clipboard=unnamedplus
set cursorline
set wildmenu
set number "relativenumber
set noerrorbells
set tabstop=4 softtabstop=4
set shiftwidth=4
set expandtab
set smartindent
set nu
set nowrap
set smartcase
set noswapfile
set nobackup
set undodir=~.vim/undodir
set undofile
set incsearch
set hlsearch
set backspace =indent,eol,start
set smartcase
set background=dark
set laststatus=2
set colorcolumn=80
highlight Colorculumn ctermbg=0 guibg=lightgrey

map <C-n> :NERDTreeToggle<CR>
let mapleader = " "
let NERDTreeShowHidden=1
let g:lightline = {
  \   'colorscheme': 'candid',
  \   'active': {
  \     'left':[ [ 'mode', 'paste' ],
  \              [ 'gitbranch', 'readonly', 'filename', 'modified' ]
  \     ]
  \   },
  \   'component_function': {
  \     'gitbranch': 'fugitive#head',
  \   }
  \}
